export interface Repo {
  id: number;
  svn_url: string;
  name: string;
  stargazers_count: number;
  watchers_count: number;
}

export interface ReposState {
  query: string;
  loading: boolean;
  repos: Repo[];
}

export const GET_REPOS = 'GET_REPOS';
export const SET_REPOS = 'SET_REPOS';

interface GetRepos {
  type: typeof GET_REPOS;
  payload: string;
}

interface SetRepos {
  type: typeof SET_REPOS;
  payload: Repo[];
}

export type ReposActionTypes = GetRepos | SetRepos;
