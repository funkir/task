import {GET_REPOS, SET_REPOS, ReposActionTypes, Repo} from './types';

export const getRepos = (query: string): ReposActionTypes => ({
  type: GET_REPOS,
  payload: query,
});

export const setRepos = (repos: Repo[]): ReposActionTypes => ({
  type: SET_REPOS,
  payload: repos,
});
