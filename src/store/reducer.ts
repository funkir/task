import {
  GET_REPOS, SET_REPOS, ReposState, ReposActionTypes,
} from './types';

const initialState: ReposState = {
  query: '',
  loading: false,
  repos: [],
};

const reducer = (state = initialState, action: ReposActionTypes): ReposState => {
  switch (action.type) {
    case GET_REPOS:
      return { ...state, query: action.payload, loading: true };
    case SET_REPOS:
      return { ...state, repos: action.payload, loading: false };
    default:
      return state;
  }
};
export default reducer;
