import { put, all, debounce } from 'redux-saga/effects';
import { GET_REPOS, ReposActionTypes } from './types';
import { setRepos } from './actions';

function* fetchRepos(action: ReposActionTypes) {
  const json = yield fetch(`https://api.github.com/search/repositories?q=${action.payload}`)
    .then(response => response.json());
  yield put(setRepos(json.items));
}

function* actionWatcher() {
  yield debounce(300, GET_REPOS, fetchRepos);
}

export default function* rootSaga() {
  yield all([
    actionWatcher(),
  ]);
}
