import React from 'react';
import { Repo } from '../store/types';

const style = {
  width: '50%',
  margin: '0 auto',
  marginTop: 10,
  padding: 5,
  border: '1px solid grey',
  borderRadius: 5,
} as React.CSSProperties;

interface RepoItemProps {
  item: Repo;
}

class RepoItem extends React.PureComponent<RepoItemProps> {
  render() {
    const { item } = this.props;

    return (
      item ?
        <div style={style}>
          <p>
            Название проекта:
            <a href={item.svn_url} target="_blank" rel="noopener noreferrer">{item.name}</a>
          </p>
          <p>
            Звезд:
            {item.stargazers_count}
          </p>
          <p>
            Подписчиков:
            {item.watchers_count}
          </p>
        </div>
        : null
    );
  }
}

export default RepoItem;



