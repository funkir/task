import React from 'react';
import { connect } from 'react-redux';
import { ReposState } from '../store/types';

const Loading = ({ loading }: {loading: boolean}) => (
  loading ?
    <div style={{ textAlign: 'center' }}>
      <h3>Загрузка...</h3>
    </div> : null
);

const mapStateToProps = (state: ReposState) => ({ loading: state.loading });

export default connect(mapStateToProps)(Loading);
