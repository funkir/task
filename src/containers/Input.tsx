import React from "react";
import { connect } from 'react-redux';
import { getRepos } from '../store/actions';
import { ReposActionTypes } from '../store/types';

interface InputProps {
  dispatchGetRepos: (query: string) => ReposActionTypes;
}

const Input = ({ dispatchGetRepos }: InputProps) => {
  function onChange(e: React.ChangeEvent<HTMLInputElement>) {
    const query = e.target.value;
    if (query.length > 2) {
      dispatchGetRepos(query);
    }
  }
  return (
    <div style={{ justifyContent: 'center', display: 'flex' }}>
      <input
        type="text"
        placeholder="Введите запрос..."
        onChange={onChange}
      />
    </div>
  );
};

const mapDispatchToProps = {
  dispatchGetRepos: (query: string) => getRepos(query),
};

export default connect(null,mapDispatchToProps)(Input);

