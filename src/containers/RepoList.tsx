import React from 'react';
import { ReposState, Repo } from '../store/types';
import { connect } from 'react-redux';
import RepoItem from './RepoItem';

const RepoList = ({ repos }: {repos: Repo[]}) => (
  <ul>
    {repos.map(repo => (
      <RepoItem
        key={repo.id}
        item={repo}
      />
    ))}
  </ul>
);

const mapStateToProps = (state: ReposState) => ({
  repos: state.repos,
});

export default connect(mapStateToProps, null)(RepoList);