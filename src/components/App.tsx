import React from 'react';
import Input from '../containers/Input';
import Loading from '../containers/Loading';
import RepoList from '../containers/RepoList';

const style = {
  display: 'flex',
  justifyContent: 'center',
  flexDirection: 'column',
  padding: 10,
} as React.CSSProperties;

const App = () => (
  <div style={style}>
    <Input />
    <Loading />
    <RepoList />
  </div>
);

export default App;
